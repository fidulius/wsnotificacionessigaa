/**
 *
 */
package co.gov.mineducacion.sigaa.web.ws.imp;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.MensajesWS;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;
import co.gov.mineducacion.sigaa.web.ws.interfaces.IWsNotificacionesSigaaLocal;
import co.gov.mineducacion.sigaa.web.ws.interfaces.IWsNotificacionesSigaaRemote;
import java.sql.Timestamp;

/**
 * @author rlozada
 *
 */
@WebService
@Stateless
public class WsNotificacionesSigaa implements IWsNotificacionesSigaaLocal, IWsNotificacionesSigaaRemote {

    /* (non-Javadoc)
     * @see co.gov.mineducacion.sigaa.web.ws.interfaces.IWsNotificacionesSigaaLocal#reportarActo(co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest)
     */
    @Override
    @WebMethod(operationName = "reportarActo")
    @WebResult(name = "GenericResponse")
    public GenericResponse reportarActo(ReportarActoRequest acto) {
        GenericResponse result = new GenericResponse();
        // TODO: Logica negocio Dependencia
        result.setCodigoRespuesta(MensajesWS.CODIGO_OK);
        result.setMensajeRespuesta(MensajesWS.MENSAJE_OK);
        return result;
    }

}
