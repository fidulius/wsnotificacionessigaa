/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.dominio;

/**
 * @author rlozada
 *
 */
public class ReportarActoRequest {
	
	// Variables de identificacion del documento en sistema origen
	
	/**
	 * Sistema origen del Documento del Acto Administrativo
	 */
	private String sistema;
	
	/**
	 * Identificador del Documento del Acto Administrativo en el sistema externo
	 */
	private String idDocSistema;
	
	// Variable representacion del Acto
	
	/**
	 * Representacion de Acto Administrativo
	 */
	private ActosAdministrativosWSDTO acto;

	/**
	 * @return the sistema
	 */
	public String getSistema() {
		return sistema;
	}

	/**
	 * @param sistema the sistema to set
	 */
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	/**
	 * @return the idDocSistema
	 */
	public String getIdDocSistema() {
		return idDocSistema;
	}

	/**
	 * @param idDocSistema the idDocSistema to set
	 */
	public void setIdDocSistema(String idDocSistema) {
		this.idDocSistema = idDocSistema;
	}

	/**
	 * @return the acto
	 */
	public ActosAdministrativosWSDTO getActo() {
		return acto;
	}

	/**
	 * @param acto the acto to set
	 */
	public void setActo(ActosAdministrativosWSDTO acto) {
		this.acto = acto;
	}
	
	

}
