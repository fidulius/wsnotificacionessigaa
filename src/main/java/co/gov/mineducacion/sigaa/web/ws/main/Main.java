/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.main;

import javax.xml.ws.Endpoint;

import co.gov.mineducacion.sigaa.web.ws.imp.WsNotificacionesSigaa;

/**
 * @author rlozada
 *
 */
public class Main {
	
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/WsNotificacionesSigaa", new WsNotificacionesSigaa());
	}

}
